<?php

namespace app\controllers;
use app\models\Category;
use app\models\Product;
use app\modules\admin\models\Brand;
use Yii;
use yii\data\Pagination;
/**
 * @property int $id
 * @property string $name
 * @property Product $products
 * @property string $content
 * @property null $keywords
 * @property float $price
 * @property integer $new
 * @property integer $sale
 * @property integer $hit
 */
class BrandController extends AppController
{
    public function actionIndex()
    {
        $brands = Brand::find()->all();

        return $this->render('index', ['brands' => $brands]);
    }

    public function actionView($id)
    {
        $query = Product::find()->where(['brand_id' => $id]);
        $brand = Brand::findOne($id);
        if (empty($brand)) {
            throw new \yii\web\HttpException(404, 'Такой brand нет ');
        }
        $pages = new Pagination([
            'totalCount'     => $query->count(),
            'pageSize'       => 10,
            'forcePageParam' => false,
            'pageSizeParam'  => false
        ]);
        $products = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        return $this->render('view',compact(
            'brand',
            'pages',
            'products'
        ));
    }
    public function actionSearch(){
        $q = trim(Yii::$app->request->get('q'));
        $this->setMeta('E-SHOPPER |Поиск: ' . $q);
        if (!$q)
            return $this->render('view');
        $query = Brand::find()->where(['like', 'name', $q]);
        $pages = new Pagination(['totalCount' =>
            $query->count(), 'pageSize' => 3, 'forcePageParam' => false, 'pageSizeParam' => false]);
        $brands = $query->offset($pages->offset)->limit($pages->limit)->all();
        return $this->render('view', compact('brands', 'pages', 'q'));
    }
}