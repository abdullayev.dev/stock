<?php

namespace app\controllers;

use app\models\Category;
use app\models\Product;
use app\modules\admin\models\Banner;
use Yii;
use yii\data\Pagination;

class CategoryController extends AppController
{
    public function actionIndex()
    {
        $hits = Product::find()
            ->where(['hit' => '1'])
            ->limit(6)
            ->all();
        $banners = Banner::find()->where(['type' => Banner::TYPE_MAIN])->all();
        $this->setMeta('E-SHOPPER');
        return $this->render('index', [
            'hits' => $hits,
            'banners' => $banners
        ]);
    }

    public function actionView($id)
    {
        $query = Product::find()->where(['category_id' => $id]);
        $category = Category::findOne($id);
        $banner = Banner::find()
            ->where(['type' => Banner::TYPE_CATEGORY])
            ->andWhere(['category_id' => $id])
            ->one();

        if (empty($category)) {
            throw new \yii\web\HttpException(404, 'Такой категорий нет ');
        }

        $pages = new Pagination([
            'totalCount' => $query->count(),
            'pageSize' => 10,
            'forcePageParam' => false,
            'pageSizeParam' => false
        ]);

        $products = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        $this->setMeta('E-SHOPPER |' . $category->name, $category->keywords, $category->description);

        return $this->render('view', compact(
            'products',
            'pages',
            'category',
            'banner'
        ));
    }

    public function actionSearch()
    {
        $q = trim(Yii::$app->request->get('q'));
        $this->setMeta('E-SHOPPER |Поиск: ' . $q);
        if (!$q)
            return $this->render('search');
        $query = Product::find()->where(['like', 'name', $q]);
        $pages = new Pagination(['totalCount' =>
            $query->count(), 'pageSize' => 3, 'forcePageParam' => false, 'pageSizeParam' => false]);
        $products = $query->offset($pages->offset)->limit($pages->limit)->all();
        return $this->render('search', compact('products', 'pages', 'q'));

    }

}
