<?php

namespace app\controllers;
use app\models\Product;
use app\modules\admin\models\Faq;
use Yii;


class FaqController extends AppController
{
public function actionIndex(){

    $faqs = Faq::find()->all();

    return $this->render('index',['faqs' => $faqs]);
}
    public function actionView($id)
    {
        $faq = Faq::findOne($id);
        if (!$faq) {
            throw new \yii\web\NotFoundHttpException('Такой faq нет ');
    }

    $query = Product::find()->where(['faq_id' => $id])->all();

    return $this->render('view', [
        'faq' => $faq,
        'products' => $query,
    ]);
}
}