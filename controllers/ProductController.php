<?php

namespace app\controllers;

use app\models\Category;
use app\models\Product;
use Yii;
/**
 * @property int $id
 * @property string $name
 * @property Product $products
 * @property string $content
 * @property null $keywords
 * @property float $price
 * @property integer $new
 * @property integer $sale
 * @property integer $hit
 */
class ProductController extends AppController
{
    public function actionView($id)
    {
        $product = Product::findOne($id);
        if (empty($product))
            throw new \yii\web\HttpException(404, 'Такой категорий нет ');

        // $product = Product::find()->with('category')->where(['id'=>$id])->limit(1)->one();
        $hits = Product::find()->where(['hit' => '1'])->limit(6)->all();
        $this->setMeta('E-SHOPPER |' . $product->name, $product->keywords, $product->description);
        return $this->render('view', ['product' => $product,'hits'=>$hits]);
    }
}