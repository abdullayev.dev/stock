 <div class="table-responsive">
     <table style="width: 100%; border: 1px solid #ddd; border-collapse: collapse;">
            <thead>
            <tr style="background: #f9f9f9;">
                <th style="padding: 8px; border: 1px solid #ddd;">Наименование</th>
                <th style="padding: 8px; border: 1px solid #ddd;">Количество</th>
                <th style="padding: 8px; border: 1px solid #ddd;">Цена</th>
                <th style="padding: 8px; border: 1px solid #ddd;">Сумма</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($session['cart'] as $id => $item): ?>
                <tr>
                    <td style="padding: 8px; border: 1px solid #ddd;"><?= $item['name'] ?></td><?= \yii\helpers\Url::to(['product/view','id'=>$id],true)?>
                    <td style="padding: 8px; border: 1px solid #ddd;"><?= $item['qty'] ?></td>
                    <td style="padding: 8px; border: 1px solid #ddd;"><?= $item['price'] ?></td>
                    <td style="padding: 8px; border: 1px solid #ddd;"><?= $item['qty'] * $item['price'] ?></td>
                </tr>
            <?php endforeach ?>
            <tr>
                <td style="padding: 8px; border: 1px solid #ddd;" colspan="3">Итого: </td>
                <td style="padding: 8px; border: 1px solid #ddd;"><?= $session['cart.qty'] ?></td>
            </tr>
            <tr>
                <td style="padding: 8px; border: 1px solid #ddd;" colspan="3">На сумму: </td>
                <td style="padding: 8px; border: 1px solid #ddd;"><?= $session['cart.sum'] ?></td>
            </tr>
<!--            <tr>-->
<!--                <td>-->
<!--                    <button type="button" class="btn btn-default" data-dismiss="modal">Продолжить покупки</button>-->
<!--                </td>-->
<!--                <td>-->
<!--                    <a href="--><?php //= \yii\helpers\Url::to(['cart/order'])?><!--" class="btn btn-success">Оформить заказ</a>-->
<!--                </td>-->
<!--                <td>-->
<!--                    <button type="button" class="btn btn-danger" onclick="clearCart()">Очистить корзину</button>-->
<!--                </td>-->
<!--            </tr>-->
            </tbody>
        </table>
    </div>

