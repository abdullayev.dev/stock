<?php
use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%banner}}`.
 */
class m230828_074934_create_banner_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%banner}}', [
            'id' => $this->primaryKey(),
            'name'=>$this->string(),
            'title'=>$this->string(),
            'image'=>$this->string(),
            'type'=>$this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%banner}}');
    }
}
