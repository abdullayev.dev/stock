<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%product}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%brand}}`
 */
class m230830_100319_add_brand_id_column_to_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%product}}', 'brand_id', $this->integer()->defaultValue(1)->notNull());

        // creates index for column `brand_id`
        $this->createIndex(
            '{{%idx-product-brand_id}}',
            '{{%product}}',
            'brand_id'
        );

        // add foreign key for table `{{%brand}}`
        $this->addForeignKey(
            '{{%fk-product-brand_id}}',
            '{{%product}}',
            'brand_id',
            '{{%brand}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%brand}}`
        $this->dropForeignKey(
            '{{%fk-product-brand_id}}',
            '{{%product}}'
        );

        // drops index for column `brand_id`
        $this->dropIndex(
            '{{%idx-product-brand_id}}',
            '{{%product}}'
        );

        $this->dropColumn('{{%product}}', 'brand_id');
    }
}
