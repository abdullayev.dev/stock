<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%banner}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%category}}`
 */
class m230911_051923_add_category_id_column_to_banner_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%banner}}', 'category_id', $this->integer());

        // creates index for column `category_id`
        $this->createIndex(
            '{{%idx-banner-category_id}}',
            '{{%banner}}',
            'category_id'
        );

        // add foreign key for table `{{%category}}`
        $this->addForeignKey(
            '{{%fk-banner-category_id}}',
            '{{%banner}}',
            'category_id',
            '{{%category}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%category}}`
        $this->dropForeignKey(
            '{{%fk-banner-category_id}}',
            '{{%banner}}'
        );

        // drops index for column `category_id`
        $this->dropIndex(
            '{{%idx-banner-category_id}}',
            '{{%banner}}'
        );

        $this->dropColumn('{{%banner}}', 'category_id');
    }
}
