<?php

namespace app\models;
use yii\db\ActiveRecord;
use yii;
class Brand extends ActiveRecord
{
    public function behaviors()
    {
        return [
            'image'=>[
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }
    public static function tableName()
    {
        return 'brand';
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brand::className(), ['id' => 'brand_id']);
    }
}