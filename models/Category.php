<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property string $name
 * @property Product $products
 */
class Category extends ActiveRecord
{
    public function behaviors()
    {
        return [
            'image'=>[
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }
    public $nameTest;
    /**
     * @var mixed|null
     */
    public $keywords;

    public static function tableName()
    {
        return 'category';
    }

    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['category_id' => 'id']);
    }
}