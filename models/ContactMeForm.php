<?php

namespace app\models;

use Yii;
use yii\base\Model;

class ContactMeForm extends Model
{
    public $name;
    public $email;
    public $message;
    public $password;
    public $phone;
    public function rules()
    {
        return [
            [['name', 'email', 'message', 'password'], 'required'],
            ['email', 'email'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Name',
            'email' => 'Email',
            'message' => 'Message',
            'password' => 'Password',
        ];
    }
}
