<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii;
/**
 * @property int $id
 * @property int $category_id
 * @property string $name
 * @property string $content
 * @property float $price
 * @property int $new
 * @property Category $category
 */
class Product extends ActiveRecord
{
    public function behaviors()
    {
        return [
            'image'=>[
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
}