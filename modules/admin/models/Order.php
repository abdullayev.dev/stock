<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $qty
 * @property float|null $sum
 * @property string|null $status
 * @property string|null $name
 * @property string|null $email
 * @property string|null $phone
 * @property string|null $address
 *
 * @property OrderItems[] $orderItems
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['qty'], 'default', 'value' => null],
            [['qty'], 'integer'],
            [['sum'], 'number'],
            [['status', 'name', 'email', 'phone', 'address'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Номер заказа '),
            'created_at' => Yii::t('app', 'Дата создания '),
            'updated_at' => Yii::t('app', 'Дата изменения '),
            'qty' => Yii::t('app', 'Кол-во '),
            'sum' => Yii::t('app', 'Сумма '),
            'status' => Yii::t('app', 'Статус '),
            'name' => Yii::t('app', 'Имя'),
            'email' => Yii::t('app', 'Е-mail '),
            'phone' => Yii::t('app', 'Телефон '),
            'address' => Yii::t('app', 'Адрес '),
        ];
    }

    /**
     * Gets query for [[OrderItems]].
     *
     * @return \yii\db\ActiveQuery|OrderItemsQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(OrderItems::class, ['order_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return OrderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrderQuery(get_called_class());
    }
}
