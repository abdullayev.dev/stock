<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property int|null $category_id
 * @property string|null $name
 * @property string|null $content
 * @property float|null $price
 * @property string|null $description
 * @property string|null $img
 * @property int|null $hit
 * @property int|null $new
 * @property int|null $sale
 * @property string|null $keywords
 *
 * @property Category $category
 * @property OrderItems[] $orderItems
 */
class Product extends \yii\db\ActiveRecord
{
    public $image;
    public $gallery;

    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'hit', 'new', 'sale'], 'default', 'value' => null],
            [['category_id', 'hit', 'new', 'sale'], 'integer'],
            [['content'], 'string'],
            [['price'], 'number'],
            [['name', 'description', 'img', 'keywords'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::class, 'targetAttribute' => ['category_id' => 'id']],
            [['image'], 'file', 'extensions' => 'png,jpg'],
             [['gallery'],'file', 'extensions' => 'png,jpg','maxFiles' => 4],
        ];

    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID товара'),
            'category_id' => Yii::t('app', 'Категории'),
            'name' => Yii::t('app', 'Наименование'),
            'content' => Yii::t('app', 'Кoнтент'),
            'price' => Yii::t('app', 'Цена'),
            'description' => Yii::t('app', 'Мета-описание'),
            'image' => Yii::t('app', 'Фото'),
            'gallery' => Yii::t('app', 'Галерея '),
            'hit' => Yii::t('app', 'Хит'),
            'new' => Yii::t('app', 'Новинка'),
            'sale' => Yii::t('app', 'Распродажа'),
            'keywords' => Yii::t('app', 'Ключевые слова'),
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $path = 'upload/store/' . $this->image->baseName . '.' . $this->image->extension;
            $this->image->saveAs($path);
            $this->attachImage($path,true);
            @unlink($path);
            return true;
        } else {
            return false;
        }
    }
    public function uploadGallery()
    {
        if ($this->validate()) {
            foreach ($this->gallery as $file){
                $path = 'upload/store/' . $file->baseName . '.' . $file->extension;
                $file->saveAs($path);
                $this->attachImage($path);
                @unlink($path);
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Gets query for [[Category]].
     *
     * @return \yii\db\ActiveQuery|CategoryQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }

    /**
     * Gets query for [[OrderItems]].
     *
     * @return \yii\db\ActiveQuery|OrderItemsQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(OrderItems::class, ['product_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return ProductQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductQuery(get_called_class());
    }
}
