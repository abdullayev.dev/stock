<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\modules\admin\models\Brand $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="brand-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'imageFiles')->fileInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList(['0'=>'Активен','1'=>'Завершен']) ?>

<!--    --><?php //= $form->field($model, 'created_at')->dropDownList(['maxlength' => true]) ?>
<!---->
<!--    --><?php //= $form->field($model, 'updated_at')->dropDownList(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
