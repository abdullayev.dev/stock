<div class="admin-default-index container">
    <h1><?= $this->context->action->uniqueId ?></h1>
    <p>
        Это просмотр содержимого для действия "<?= $this->context->action->id ?>".
        Действие принадлежит контроллеру "<?= get_class($this->context) ?>"
        in the "<?= $this->context->module->id ?>" module.
    </p>
    <p>
        Вы можете настроить эту страницу, отредактировав следующий файл:<br>
        <code><?= __FILE__ ?></code>
    </p>
</div>
