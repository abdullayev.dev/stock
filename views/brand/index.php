<?php

/** @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

?>
<section id="slider">
    <div class="container">
        <div class="row">
<?php if(!empty($brands)): ?>
            <?php foreach ($brands as $brand): ?>
                <div class="col-sm-3">
                    <div class="product-image-wrapper">
                        <div class="single-products">
                            <div class="productinfo text-center">
                                <?= Html::img("@web/{$brand->image}", ['alt' => $brand->name]) ?>
                                <p><a href="<?= Url::to(['brand/view', 'id' => $brand->id]) ?>"><?= $brand->name ?></a></p>
                                <a href="<?= Url::to(['cart/add', 'id' => $brand->id]) ?>" data-id="<?= $brand->id ?>"</a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <?php endif; ?>
    </div>
</section>

