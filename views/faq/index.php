<?php

/** @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

?>
<section id="slider">
    <div class="container">
        <h2>Часто задаваемые вопросы.?</h2>
        <div class="accordion">
            <?php if (!empty($faqs)): ?>
                <?php foreach ($faqs as $faq): ?>
                    <div class="accordion-item">
                        <button id="accordion-button-<?= $faq->id ?>"
                                aria-expanded="false"><span class="accordion-title">
               <?= $faq->name ?></span><span class="icon" aria-hidden="true"></span></button>
                        <div class="accordion-content">
                            <p><?= $faq->content ?></p>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</section>


