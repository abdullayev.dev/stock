<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-contact container">
    <h1 style="color: #0a53be"><?= Html::encode($this->title) ?></h1>

    <h5 style="color: #ba8b00">
        Если у вас есть какие-либо вопросы или сомнения,пожалуйста, <br>
        заполните следующую форму, чтобы связаться с нами.
        Спасибо!
    </h5>

    <?php if (Yii::$app->session->hasFlash('success')): ?>

        <div class="alert alert-success">
            <?= Yii::$app->session->getFlash('success') ?>
        </div>

    <?php else: ?>

        <div class="row">
            <div class="col-lg-5">

                <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'email') ?>

                <?= $form->field($model, 'message')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <div class="form-group">
                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>

    <?php endif; ?>

</div>
