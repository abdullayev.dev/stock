<?php

/** @var yii\web\View $this */
/** @var string $name */
/** @var string $message */
/** @var Exception$exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error container">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>
<?php debug($message)?>
    <p style="color: red">
        Вышеупомянутая ошибка произошла, когда веб-сервер обрабатывал ваш запрос.
    </p>
    <p style="color: red">
        Пожалуйста, свяжитесь с нами, если вы считаете, что это ошибка сервера. Спасибо
    </p>

</div>
